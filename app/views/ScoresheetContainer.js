var React = require('react');
var Scoresheet = require('../components/scoresheet/index');

var ScoresheetContainer = React.createClass({

	render: function() {
		return (
			<Scoresheet />
		)
	}
});

module.exports = ScoresheetContainer;