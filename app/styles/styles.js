var Styles = {
	
	inline: {
		display: 'inline-block'
	},
	verticalAlign: {
		top: {
			verticalAlign: 'top'
		},
		bottom: {
			verticalAlign: 'bottom'
		}
	},
	border: {
		black: {
			all: {
				border: '1px solid black'
			},
			bottom: {
				borderBottom: '1px solid black'
			},
			left: {
				borderLeft: '1px solid black'
			}
		}
	},
	position: {
		relative: {
			position: 'relative'
		},
		absolute: {
			position: 'absolute'
		}
	},
	float: {
		left: {
			float: 'left'
		},
		right: {
			float: 'right'
		}
	},
	textAlign: {
		right: {
			textAlign: 'right'
		},
		center: {
			textAlign: 'center'
		}
	},
	width: {
		full: {
			width: '100%'
		}
	}
};

module.exports = Styles;