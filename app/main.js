var React = require('react');
var ReactDom = require('react-dom');
var IndexContainer = require('./views/IndexContainer');

ReactDom.render(
	
	<IndexContainer /> ,
	document.getElementById('app')
);