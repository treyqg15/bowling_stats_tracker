var React = require('react');
var Styles = require('../../../../styles/styles');

var Pins = React.createClass({

	getStyles: function() {

		return {
			border: {
				border: '1px solid black'
			}
		};
	},
	render: function() {
		return (
			<div>
				<div style={Object.assign({},this.getStyles().border)}>
					Pin Pics
				</div>
			</div>
		)
	}
});

module.exports = Pins;