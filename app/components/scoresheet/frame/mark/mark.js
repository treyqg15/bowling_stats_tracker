var React = require('react');
var Styles = require('../../../../styles/styles');

var Mark = React.createClass({

	getStyles: function() {
		
		return {
			markFrame: {
				borderLeft: '1px solid black',
				borderBottom: '1px solid black',
				color: 'red',
				float: 'right',
				width: 25,
				height: '100%',
				textAlign: 'center'
			},
			markFrameContainer: {
				height: 25,
				borderTop: 0,
				borderLeft: '1px solid black',
				borderRight: '1px solid black',
				borderBottom: 0
			},
			scoreContainer: {
				position: 'relative',
				borderLeft: '1px solid black',
				borderRight: '1px solid black',
				borderBottom: '1px solid black',
				height: 25,
				textAlign: 'center'
			},
			score: {
				position: 'absolute',
				bottom: 0,
				width: '100%'
			},
			margin: {
				marginRight: 6
			}
		}
	},
	render: function() {
		return (
			<div>
				<div style={Object.assign({},Styles.position.relative,this.getStyles().markFrameContainer)}>
					<div style={Object.assign({},this.getStyles().markFrame)}>
						<span>/</span>
					</div>
					<div style={Object.assign({},Styles.float.right,this.getStyles().margin)}>
						<span>9</span>
					</div>
				</div>
				<div style={Object.assign({},Styles.textAlign.center,this.getStyles().scoreContainer)}>
					<div style={Object.assign({},this.getStyles().score)}>
						100
					</div>
				</div>
			</div>
		)
	}
});

module.exports = Mark;