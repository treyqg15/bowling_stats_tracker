var React = require('react');
var Styles = require('../../../../styles/styles');

var Button = React.createClass({

	getStyles: function() {

		return {
			border: {
				border: '1px solid black'
			}
		}
	},
	render: function() {
		return (
			<div>
				<div style={Object.assign({},this.getStyles().border,Styles.width.full)}>
					<input type="submit" value="Edit Frame"/>
				</div>
			</div>
		)
	}
});

module.exports = Button;