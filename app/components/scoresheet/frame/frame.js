var React = require('react');
var Styles = require('../../../styles/styles');
var Pins = require('./pins/pins');
var Mark = require('./mark/mark');
var Button = require('./button/button');

var Frame = React.createClass({

	render: function() {
		
		return (
			<div>
				<Pins />
				<Mark />
				<Button />
			</div>
		)
	}
});

module.exports = Frame;