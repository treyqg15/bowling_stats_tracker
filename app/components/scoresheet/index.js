var React = require('react');
var Styles = require('../../styles/styles');
var Name = require('./name/name');
var Average = require('./average/average');
var Handicap = require('./handicap/handicap');
var Frame = require('./frame/frame');

var Index = React.createClass({
	
	getStyles: function() {
		
		return {
			frameContainer: {
				width: 80
			}
		};
	},
	render: function() {

		var getFrames = (function () {

			var arr = [];
			for(var i = 1; i < 13; ++i) {
				arr.push(
					<div key={i} style={Object.assign({},Styles.inline,this.getStyles().frameContainer)}>
						<Frame />
					</div>
				);
			}

			return arr;
		}).bind(this);

		getFrames = getFrames();

		return (

			<div>
				<div style={Object.assign({},Styles.inline,Styles.verticalAlign.bottom,Styles.border.black.all)}>
					Name
				</div>
				<div style={Object.assign({},Styles.inline,Styles.verticalAlign.bottom,Styles.border.black.all)}>
					Average
				</div>
				<div style={Object.assign({},Styles.inline,Styles.verticalAlign.bottom,Styles.border.black.all)}>
					Handicap
				</div>
				<div style={Object.assign({},Styles.inline)}>
					{getFrames}
				</div>
			</div>
		)
	}
});

module.exports = Index;