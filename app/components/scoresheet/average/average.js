var React = require('react');
var Styles = require('../../../styles/styles');

var Average = React.createClass({

	getStyles: function() {

		return {
			font: {
				fontSize: 16,
				fontWeight: 'bold'
			},
			border: {
				borderLeft: '1px solid black',
				borderBottom: '1px solid black',
				borderTop: '1px solid black'
			},
			height: {
				height: '100%'
			}
		}
	},
	render: function() {
		return (
			<div style={Object.assign({},this.getStyles().border,this.getStyles().height)}>
				<div style={Object.assign({},Styles.border.black.bottom,Styles.textAlign.center)}>
					<span style={Object.assign({},this.getStyles().font)}>AVG</span>
				</div>
				<div style={Object.assign({},Styles.textAlign.center)}>
					200
				</div>
			</div>
		)
	}
});

module.exports = Average;