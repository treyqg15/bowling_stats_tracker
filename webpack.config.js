var HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackPluginConfig = new HtmlWebpackPlugin({

	template: __dirname + '/app/index.html',
	filename: 'index.html',
	inject: 'body'
});

//var nodeExternals = require('webpack-node-externals');

module.exports = {

	entry: [
		'./app/main.js'
	],
	output: {
		path: __dirname + '/dist',
		filename: 'index_bundle.js'
	},
	module: {
		loaders: [
			{test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
			{test: /\.json$/, loader: 'json-loader'}
		],
    	noParse: /node_modules\/json-schema\/lib\/validate\.js/
	},
	node: {
		net: 'empty',
		child_process: 'empty',
		fs: 'empty',
		tls: 'empty'
	},
	//target: 'node', // in order to ignore built-in modules like path, fs, etc.
    //externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
	//debug: true,
	plugins: [HtmlWebpackPluginConfig]
}